#!/bin/bash

# TP-Link WN822N V5 8192eu -vs- rtl8xxxu

apt install bc
unzip https://static.tp-link.com/2018/201805/20180514/TP-Link_Driver_Linux_series8_beta.zip
cd tplink

make clean
make
make install
modprobe 8192eu
echo "blacklist rtl8xxxu" > /etc/modprobe.d/rtl8xxxu-blacklist.conf

# lsmod
# lsusb
# lshw -class network
# ln -s /lib/modules/<>/kernel/drivers/net/wireless/

# https://scdas141.wordpress.com/2017/01/28/how-to-compile-mangertl8192eu-linux-driver-driver/
# https://github.com/Mange/rtl8192eu-linux-driver/issues/46#issuecomment-323710229
