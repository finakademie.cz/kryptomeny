rd /s /q "c:\mining" & mkdir "c:\mining" & cd "c:\mining"
git clone https://github.com/xmrig/xmrig.git
git clone https://github.com/xmrig/xmrig-deps.git
cd "c:\mining\xmrig" & mkdir build & cd build
"C:\Program Files\Git\git-bash" -c "sed -ri 's|DonateLevel .*|DonateLevel = 0;|g' ../src/donate.h"
"C:\Program Files\CMake\bin\cmake" .. -G "Visual Studio 15 2017 Win64" -DXMRIG_DEPS="c:\mining\xmrig-deps\msvc2017\x64" -DWITH_HTTPD=OFF
"C:\Program Files\CMake\bin\cmake" --build . --config Release --target xmrig
move "c:\mining\xmrig\build\Release\xmrig.exe" "c:\mining\xmrig.exe"
cd "c:\mining"
