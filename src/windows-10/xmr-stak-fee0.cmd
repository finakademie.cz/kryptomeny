rd /s /q "c:\mining" & mkdir "c:\mining" & cd "c:\mining"
git clone https://github.com/fireice-uk/xmr-stak.git
wget https://github.com/fireice-uk/xmr-stak-dep/releases/download/v2/xmr-stak-dep.zip
powershell.exe -NoP -NonI -Command "Expand-Archive '.\xmr-stak-dep.zip' '.\'"
del xmr-stak-dep.zip
cd "c:\mining\xmr-stak"
"C:\Program Files\Git\git-bash" -c "sed -ri 's|fDevDonationLevel .*|fDevDonationLevel = 0;|g' xmrstak/donate-level.hpp"
"C:\Program Files (x86)\Microsoft Visual Studio\2017\BuildTools\Common7\Tools\VsMSBuildCmd.bat"
mkdir build & cd build
set CMAKE_PREFIX_PATH=c:\mining\xmr-stak-dep\hwloc;c:\mining\xmr-stak-dep\libmicrohttpd;c:\mining\xmr-stak-dep\openssl
"C:\Program Files\CMake\bin\cmake" -G "Visual Studio 15 2017 Win64" -T v141,host=x64 .. -DMICROHTTPD_ENABLE=ON -DCUDA_ENABLE=OFF -DOpenCL_INCLUDE_DIR="C:\Program Files (x86)\AMD APP SDK\3.0\include" -DOpenCL_LIBRARY="C:\Program Files (x86)\AMD APP SDK\3.0\lib\x86_64\libOpenCL.a"
"C:\Program Files\CMake\bin\cmake" --build . --config Release --target install
cd bin\Release
copy c:\mining\xmr-stak-dep\openssl\bin\* .
for /f %F in ('dir /b /a-d ^| findstr /vile ".exe .dll"') do del "%F"
move * "c:\mining"
cd "c:\mining"
