#!/bin/bash

build() {
	apt update
	apt upgrade -y
	apt install cmake libmicrohttpd-dev libhwloc-dev ocl-icd-opencl-dev
	apt autoremove
	mkdir -p /hive/custom/xmrstak-git
	cd /hive/custom/xmrstak-git
	git clone https://github.com/fireice-uk/xmr-stak.git
	sed -ri "s|fDevDonationLevel .*|fDevDonationLevel = 0;|g" xmr-stak/xmrstak/donate-level.hpp
	cat xmr-stak/xmrstak/donate-level.hpp
	cd /hive/custom/xmrstak-git && rm -rf xmr-stak/build && mkdir xmr-stak/build && cd xmr-stak/build
	cmake .. -DCUDA_ENABLE=OFF -DOpenCL_INCLUDE_DIR=/opt/amdgpu-pro/lib/x86_64-linux-gnu -DOpenCL_LIBRARY=/opt/amdgpu-pro/lib/x86_64-linux-gnu/libOpenCL.so
	make install

	# replace
	mv -f bin/xmr-stak bin/xmr-stak-fee0
	mv -f bin/* /hive/xmr-stak/fireice-uk/
	ln -sf xmr-stak-fee0 /hive/xmr-stak/fireice-uk/xmr-stak

	# clean
	cd /hive/custom && rm -rf xmrstak-git && ls -al
}

build;
