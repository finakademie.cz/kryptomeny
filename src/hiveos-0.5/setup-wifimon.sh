#!/bin/bash

echo -e "# Set as primary network\nauto wlan0\niface wlan0 inet dhcp" > /etc/network/interfaces.d/wlan0
echo -e '#!/bin/bash
((count = 5))
while [[ $count -ne 0 ]] ; do
	# skript predpoklada ip adresu routeru 192.168.2.1
	ping -c 1 192.168.2.1
	rc=$?
	if [[ $rc -eq 0 ]] ; then
		((count = 1))
    fi
	((count = count - 1))
done

if [[ $rc -eq 0 ]] ; then
    exit 0
else
	/sbin/iwconfig wlan0 txpower off
	sleep 15
	/sbin/iwconfig wlan0 txpower auto
	sleep 5
	/sbin/ifup --force "wlan0"
fi
' > /usr/local/bin/wifimon
chmod +x /usr/local/bin/wifimon
echo -e "\n*/1 * * * * /usr/bin/sudo -H /usr/local/bin/wifimon > /dev/null 2>&1" >> /hive/etc/crontab.root
