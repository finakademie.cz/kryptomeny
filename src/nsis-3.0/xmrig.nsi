!include nsDialogs.nsh
!include nsArray.nsh
!include Sections.nsh
!include StrFunc.nsh
!include WinVer.nsh
!include x64.nsh

!define /date DATESTAMP "%y%m%d"
!define INSTNAME "Xmrig"
!define INSTSIZE 5000

!define PACKAGEWXP "..\..\bin\windows-10\xmrig-fee0-32bit"
!define PACKAGEX86 "..\..\bin\windows-10\xmrig-fee0-32bit"
!define PACKAGEX64 "..\..\bin\windows-10\xmrig-fee0-64bit"

!define OUTFILE "..\..\bin\nsis-3.0\xmrig.exe"

Name "xmrig 2.6.4 build ${DATESTAMP}"
Icon "fa.ico"
OutFile "${OUTFILE}"
LoadLanguageFile "${NSISDIR}\Contrib\Language files\Czech.nlf"

LicenseText "Před instalací programu $(^NameDA) si prosím přečtěte následující licenční ujednání."
LicenseData "manifest.txt"
LicenseBkColor /windows

ShowInstDetails show
RequestExecutionLevel admin

Page license
Page components
Page directory

Page instfiles
UninstPage instfiles


Section "!$(^NameDA)" 0
	SectionIn RO

	SetOutPath $INSTDIR
	File "manifest.txt"

	; Protocol Lanucher
	${If} ${AtMostWinXP}
		DetailPrint "Instaluji balíček pro: Windows XP (32bit)"
		File /r "${PACKAGEWXP}\*"
	${ElseIf} ${RunningX64}
		DetailPrint "Instaluji balíček pro: Windows Vista+ (64bit)"
		File /r "${PACKAGEX64}\*"
	${Else}
		DetailPrint "Instaluji balíček pro: Windows Vista+ (32bit)"
		File /r "${PACKAGEX86}\*"
	${EndIf}

	; Start Menu
	CreateDirectory "$SMPROGRAMS\${INSTNAME}"
	CreateShortCut "$SMPROGRAMS\${INSTNAME}\Xmrig.lnk" "$INSTDIR\xmrig.exe" "" "$INSTDIR\xmrig.exe" 0 SW_SHOWNORMAL
	CreateShortCut "$SMPROGRAMS\${INSTNAME}\Manifest.lnk" "$INSTDIR\manifest.txt"
	CreateShortCut "$SMPROGRAMS\${INSTNAME}\Uninstall.lnk" "$INSTDIR\uninstall.exe" "" "$INSTDIR\uninstall.exe" 0

	; Desktop Play
	CreateShortCut "$DESKTOP\Xmrig.lnk" "$INSTDIR\xmrig.exe" "" "$INSTDIR\xmrig.exe" 0 SW_SHOWNORMAL

	; Run As Administrator
	ShellLink::SetRunAsAdministrator "$DESKTOP\Xmrig.lnk"
	Pop $0
	ShellLink::SetRunAsAdministrator "$SMPROGRAMS\${INSTNAME}\Xmrig.lnk"
	Pop $0

	; Enable SeLock Pages Memory
	UserMgr::GetCurrentUserName
	Pop $0
	UserMgr::AddPrivilege "$0" "SeLockMemoryPrivilege"
	Pop $0

	; Run As Service with Administrator Rights
	; shortcut to start and stop service


	WriteUninstaller "$INSTDIR\uninstall.exe"
SectionEnd

Section "Uninstall"
	SetDetailsPrint none
	SetDetailsPrint both
	Delete "$DESKTOP\Xmrig.lnk"

	RMDir /r "$SMPROGRAMS\${INSTNAME}"
	RMDir /r "$INSTDIR"
SectionEnd

Function .onInit
	${IfNot} ${AtLeastWinXP}
		MessageBox MB_YESNO|MB_ICONEXCLAMATION "Minimální požadavky na OS jsou Windows XP a vyšší!$\nPokračovat v instalaci?" IDYES trueAtLeastWinXP IDNO falseAtLeastWinXP
		falseAtLeastWinXP:
			Quit
		trueAtLeastWinXP:
	${EndIf}

	; Vychozi hodnoty vlastniho nastaveni
	StrCpy $INSTDIR "$PROGRAMFILES64\${INSTNAME}"

	SectionSetSize 0 ${INSTSIZE}
FunctionEnd