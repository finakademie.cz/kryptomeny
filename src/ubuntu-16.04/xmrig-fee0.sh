#!/bin/bash

build() {
	add-apt-repository -y ppa:jonathonf/gcc-7.1
	apt-get -y update
	apt-get -y install gcc-7 g++-7 unzip
	apt-get -y install git build-essential cmake libuv1-dev
	apt-get -y autoremove
	wget -O /tmp/xmrig.zip https://github.com/xmrig/xmrig/archive/master.zip 2> /dev/null
	unzip -q /tmp/xmrig.zip -d "/tmp/xmrig"
	rm -f /tmp/xmrig.zip
	cd "/tmp/xmrig/$(ls /tmp/xmrig)"
	sed -ri "s|DonateLevel .*|DonateLevel = 0;|g" src/donate.h
	mkdir build && cd build && cmake .. -DCMAKE_C_COMPILER=gcc-7 -DCMAKE_CXX_COMPILER=g++-7 -DWITH_HTTPD=OFF
	make
	killall xmrig
	rm ~/xmrig.log
	mv xmrig ~/xmrig
	echo 32 > /proc/sys/vm/nr_hugepages
	~/xmrig --config=~/config.json
	cd ~/
	rm -R /tmp/xmrig
	cat ~/xmrig.log
}

build;
