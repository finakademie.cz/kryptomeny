#!/bin/bash

for NAME in $@; do
	ps ax | grep ${NAME} | grep -v grep | grep -v bash > /dev/null
	if [ ! $? -eq 0 ]; then
		echo "Service ${NAME} is DOWN"
		if [ ! -e /tmp/${NAME}.down ]; then
			echo "$(date)" > /tmp/${NAME}.down
			echo "Service ${NAME} is DOWN from" $(date +"%F %X") | mail -s "cron" root
		fi
		if [ ${NAME} == "xmrig" ]; then
			# skript predpoklada, ze program xmrig je ulozen ve slozce /root
			sudo /root/xmrig --config=/root/config.json
		fi
	else
		echo "Service ${NAME} is UP"
		if [ -e /tmp/${NAME}.down ]; then
			rm -f /tmp/${NAME}.down
			echo "Service ${NAME} is UP from" $(date +"%F %X") | mail -s "cron" root
		fi
	fi
done
